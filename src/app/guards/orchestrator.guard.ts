/**
 * This guard verifies if the user trying to get into the orchestrator parts of the app is indeed an orchestrator.
 * An orchestrator must log in the same way as any user, but if the messge from the server comes with a special
 * orchestrator tag then the user is redirected to '/orchestrator' where it's verified by this guard.
 * 
 * An orchestrator token is valid only for 2 hours.
 */

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrchestratorGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
  
}
