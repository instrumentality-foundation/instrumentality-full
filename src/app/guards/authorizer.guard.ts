import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from "rxjs/operators";
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { constants } from '../constants';
import { NotifierService } from "../services/notifier.service";

@Injectable({
  providedIn: 'root'
})
export class AuthorizerGuard implements CanActivate {

  private resp: boolean;

  constructor(
    private http: HttpClient,
    private router: Router,
    private notifier: NotifierService
  ) {}

  private verify(username: string, token: string) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post(
      constants.iroha_network + '/account/verify',
      {
        username: username,
        token: token
      },
      {
        headers: headers,
        observe: 'response',
        responseType: 'json'
      }
    ).pipe(
      map((response: HttpResponse<any>) => {
        let status = response.body.status;

        if (status === "success") {
          return true;
        }
      })
    ).pipe(catchError(val => {
        this.notifier.notifyUIkit("Your session has expired", 'danger');
        localStorage.removeItem('user');
        return of(this.router.parseUrl("/landing/home"));
    }))
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if(localStorage.getItem('user') != null) {
      let token: string = JSON.parse(localStorage.getItem('user')).token;
      return this.verify(next.params.account, token)
    }
    else {
      return of(this.router.parseUrl('/landing/home'));
    }
  }
  
}
