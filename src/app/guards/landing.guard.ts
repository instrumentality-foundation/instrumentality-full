import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { constants } from "../constants";

@Injectable({
  providedIn: 'root'
})
export class LandingGuard implements CanActivate {

  constructor(
    private router: Router,
    private location: Location,
    private http: HttpClient
  ){}

  private async isOrchestrator(account: string, token: string) {
    return await this.http.post(
      constants.iroha_network + '/is_orchestrator',
      {
        account: account,
        token: token
      },
      {
        responseType: 'json',
        observe: 'response'
      }
    ).toPromise()
    .then(response => {
      console.log(response.body);
      return true;
    })
    .catch(err => {
      console.error(err.error);
      return false
    })
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    let user = JSON.parse(localStorage.getItem('user'));

    if (user) {
      let account = user.username;
      let token = user.token;

      if (this.isOrchestrator(account, token)) {
        return of(this.router.parseUrl('/orchestrator'));
      }
      else {
        return of(this.router.parseUrl('/' + account + '/dashboard'))
      }
    }
    else {
      return true;
    }

  }
  
}
