import { TestBed } from '@angular/core/testing';

import { OrchestratorGuard } from './orchestrator.guard';

describe('OrchestratorGuard', () => {
  let guard: OrchestratorGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(OrchestratorGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
