import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizerGuard } from './guards/authorizer.guard';
import { OrchestratorGuard } from "./guards/orchestrator.guard";
import { DeactivateGuard } from "./guards/deactivate.guard";
import { LandingGuard } from "./guards/landing.guard";

const routes: Routes = [
  { 
    path: 'orchestrator', 
    loadChildren: () => import('./orchestrator-dashboard/orchestrator-dashboard.module').then( m => m.OrchestratorDashboardModule), 
    canActivate: [OrchestratorGuard],
    canDeactivate: [DeactivateGuard]
  },
  { 
    path: 'landing', 
    loadChildren: () => import('./landing/landing.module').then(m => m.LandingModule),
    canActivate: [LandingGuard]
  },
  { 
    path: ':account/dashboard', 
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule), 
    canActivate: [AuthorizerGuard],
    canDeactivate: [DeactivateGuard]
  },
  { path: '', redirectTo: 'landing', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
