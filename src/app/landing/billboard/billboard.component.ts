import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'billboard-comp',
  templateUrl: './billboard.component.html',
  styleUrls: ['./billboard.component.less']
})
export class BillboardComponent implements OnInit {

  @Input() title : string;
  @Input() catchphrase : string;

  constructor() { }

  ngOnInit(): void {
  }

}
