import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'feature-comp',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.less']
})
export class FeatureComponent implements OnInit {

  @Input() image : string;
  @Input() description : string;
  @Input() title : string;

  constructor() { }

  ngOnInit(): void {
  }

}
