import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { AuthenticatorService } from "../../services/authenticator.service";
import { SpinnerCombo } from "../../services/spinner-controller.service";

import { faBars } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'landing-navbar-comp',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit, AfterViewInit {

  faBars = faBars;
  public signupUser;
  public loginUser;
  public loginKey;

  @ViewChild('signupButton', {static: true}) signupButton : ElementRef;
  @ViewChild('signupSpinner', {static: true}) signupSpinner : ElementRef;
  @ViewChild('loginButton', {static: true}) loginButton : ElementRef;
  @ViewChild('loginSpinner', {static: true}) loginSpinner : ElementRef;

  constructor(
    private authService : AuthenticatorService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {

  }

  public signup(event: Event) {
    // Build the spinner combo
    let spinnerCombo = new SpinnerCombo(this.signupButton, this.signupSpinner);

    let el = event.target as HTMLElement;
    let modal = el.parentElement.parentElement.parentElement;

    // Call the authentication service
    this.authService.createAccount(this.signupUser, modal, spinnerCombo);
  }

  public login() {
    // Build the spinner combo
    let spinnerCombo = new SpinnerCombo(this.loginButton, this.loginSpinner);

    // Call authentication service
    this.authService.authenticate(this.loginUser, this.loginKey, spinnerCombo);
  }

}
