import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoFsComponent } from './info-fs.component';

describe('InfoFsComponent', () => {
  let component: InfoFsComponent;
  let fixture: ComponentFixture<InfoFsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoFsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoFsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
