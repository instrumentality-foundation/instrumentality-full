import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'info-panel-comp',
  templateUrl: './info-panel.component.html',
  styleUrls: ['./info-panel.component.less']
})
export class InfoPanelComponent implements OnInit {

  @Input() image : string;
  @Input() description : string;
  @Input() title : string;
  @Input() catchphrase : string;

  constructor() { }

  ngOnInit(): void {
  }

}
