import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoBlockchainComponent } from './info-blockchain.component';

describe('InfoBlockchainComponent', () => {
  let component: InfoBlockchainComponent;
  let fixture: ComponentFixture<InfoBlockchainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoBlockchainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoBlockchainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
