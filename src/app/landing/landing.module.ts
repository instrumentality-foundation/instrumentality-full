import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

/* Importing fontawesome module */
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { BillboardComponent } from './billboard/billboard.component';
import { FeatureComponent } from './feature/feature.component';
import { AboutComponent } from './about/about.component';
import { InfoPanelComponent } from './info-panel/info-panel.component';
import { InfoBlockchainComponent } from './info-blockchain/info-blockchain.component';
import { InfoFsComponent } from './info-fs/info-fs.component';
import { AuthenticatorService } from '../services/authenticator.service';

@NgModule({
  declarations: [LandingComponent, NavbarComponent, HomeComponent, BillboardComponent, FeatureComponent, AboutComponent, InfoPanelComponent, InfoBlockchainComponent, InfoFsComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    LandingRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [AuthenticatorService]
})
export class LandingModule { }
