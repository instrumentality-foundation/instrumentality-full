import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrchestratorDashboardComponent } from "./orchestrator-dashboard.component";

const routes: Routes = [
  {
    path: '',
    component: OrchestratorDashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrchestratorDashboardRoutingModule { }
