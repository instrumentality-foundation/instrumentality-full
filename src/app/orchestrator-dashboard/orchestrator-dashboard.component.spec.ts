import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrchestratorDashboardComponent } from './orchestrator-dashboard.component';

describe('OrchestratorDashboardComponent', () => {
  let component: OrchestratorDashboardComponent;
  let fixture: ComponentFixture<OrchestratorDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrchestratorDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrchestratorDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
