import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { IrohaCommand } from "../utils/iroha-command";

@Component({
  selector: 'orchestrator-command-card',
  templateUrl: './command-card.component.html',
  styleUrls: ['./command-card.component.less']
})
export class CommandCardComponent implements OnInit {

  @Input() command : IrohaCommand;
  @Input() commandButtonText: string;

  public domain_pattern = /^[A-Za-z0-9]{1,64}$/;
  public asset_pattern = /^[a-z_0-9]{1,32}$/;

  constructor() { }

  ngOnInit(): void {
  }

}
