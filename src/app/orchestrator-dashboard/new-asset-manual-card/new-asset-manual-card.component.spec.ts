import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAssetManualCardComponent } from './new-asset-manual-card.component';

describe('NewAssetManualCardComponent', () => {
  let component: NewAssetManualCardComponent;
  let fixture: ComponentFixture<NewAssetManualCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAssetManualCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAssetManualCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
