import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { CommandCardComponent } from '../command-card/command-card.component';
import { NotifierService } from 'src/app/services/notifier.service';
import { SpinnerCombo } from 'src/app/services/spinner-controller.service';
import { IrohaResponse } from "../utils/iroha-command";

@Component({
  selector: 'orchestrator-new-asset-manual-card',
  templateUrl: './new-asset-manual-card.component.html',
  styleUrls: ['./new-asset-manual-card.component.less',
  '../command-card/command-card.component.less']
})
export class NewAssetManualCardComponent extends CommandCardComponent implements OnInit {

  @ViewChild('frontCard', {static: true}) frontCard : ElementRef; 
  @ViewChild('backCard', {static: true}) backCard : ElementRef;
  @ViewChild('sendButton', {static: true}) sendButton: ElementRef;
  @ViewChild('sendSpinner', {static: true}) sendSpinner: ElementRef;

  assetName: any;
  assetPrecision: any;

  private spinnerCombo: SpinnerCombo;

  constructor(
    private notifier: NotifierService
  ) { 
    super()
  }

  ngOnInit(): void {
  }

  flipCard() {
    if (this.frontCard.nativeElement.style.display != "none") {
      this.frontCard.nativeElement.style.display = "none";
      this.backCard.nativeElement.style.display = "block";
      this.frontCard.nativeElement.classList.add('uk-animation-scale-down');
    }
    else {
      this.frontCard.nativeElement.style.display = "block";
      this.backCard.nativeElement.style.display = "none";
    }
  }

  async createAsset() {
    /** Testing for wrong inputs */
    if(this.asset_pattern.test(this.assetName) === false) {
      this.notifier.notifyUIkit("Asset name should be small letters, '_' and numbers only", 'danger');
      return;
    }

    if (this.assetPrecision < 0 || this.assetPrecision > 255) {
      this.notifier.notifyUIkit("Precision should be between 0 and 255", 'danger');
      return;
    }

    /** Toggle the combo spinner */
    this.spinnerCombo = new SpinnerCombo(this.sendButton, this.sendSpinner);
    this.spinnerCombo.toggle();

    /** Make the request */
    let account = JSON.parse(localStorage.getItem('user'));

    try {
      let response = await this.command.request(account.username, account.token, this.assetName, this.assetPrecision);
    
      let irohaResponse = new IrohaResponse();
      irohaResponse = response.body;

      console.log(irohaResponse);
      this.notifier.notifyUIkit(irohaResponse.msg, 'success');
    }
    catch(reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, 'danger');
    }
    finally {
      this.spinnerCombo.toggle();
    }
  }

}
