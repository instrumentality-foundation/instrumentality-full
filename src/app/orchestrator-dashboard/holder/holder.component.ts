import { Component, OnInit } from '@angular/core';
import { IrohaCreateCompany } from "../utils/iroha-create-company";
import { IrohaCreateAsset } from "../utils/iroha-create-asset";
import { IrohaMintAsset } from "../utils/iroha-mint-asset";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'orchestrator-holder',
  templateUrl: './holder.component.html',
  styleUrls: ['./holder.component.less']
})
export class HolderComponent implements OnInit {

  createCompanyCommand = new IrohaCreateCompany(this.http)
  createAssetCommand = new IrohaCreateAsset(this.http);
  mintAssetCommand = new IrohaMintAsset(this.http);

  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
  }

}
