import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommandCardComponent } from '../command-card/command-card.component';
import { NotifierService } from 'src/app/services/notifier.service';
import { SpinnerCombo } from 'src/app/services/spinner-controller.service';

@Component({
  selector: 'orchestrator-company-card',
  templateUrl: './company-card.component.html',
  styleUrls: [
    './company-card.component.less', 
    '../command-card/command-card.component.less']
})
export class CompanyCardComponent extends CommandCardComponent implements OnInit {

  @ViewChild('frontCard', {static: true}) frontCard : ElementRef; 
  @ViewChild('backCard', {static: true}) backCard : ElementRef;
  @ViewChild('sendButton', {static: true}) sendButton: ElementRef;
  @ViewChild('sendSpinner', {static: true}) sendSpinner: ElementRef;

  public companyNameInput: any;
  private spinnerCombo: SpinnerCombo;

  constructor(
    private notifier: NotifierService,
  ) { 
    super();
  }

  ngOnInit(): void {
  }

  flipCard() {
    if (this.frontCard.nativeElement.style.display != "none") {
      this.frontCard.nativeElement.style.display = "none";
      this.backCard.nativeElement.style.display = "block";
      this.frontCard.nativeElement.classList.add('uk-animation-scale-down');
    }
    else {
      this.frontCard.nativeElement.style.display = "block";
      this.backCard.nativeElement.style.display = "none";
    }
  }

  async registerCompany() {
    if (this.domain_pattern.test(this.companyNameInput) != true) {
      console.error('Wrong company name format!');
      this.notifier.notifyUIkit("Use only letters and numbers", "danger");
      return;
    }
    else {
      // Creating spinner combo
      this.spinnerCombo = new SpinnerCombo(this.sendButton, this.sendSpinner);
      this.spinnerCombo.toggle();

      // Getting account details
      let orchestrator = JSON.parse(localStorage.getItem('user'));

      // Http request to python server
      try{
        let response = await this.command.request(orchestrator.username, orchestrator.token, this.companyNameInput);

        let irohaResponse = response.body;

        if (irohaResponse.status == 'success') {
          console.log(irohaResponse);
          this.notifier.notifyUIkit(irohaResponse.msg, 'success');
        }
      }
      catch(ex) {
        console.error(ex.error);
        this.notifier.notifyUIkit(ex.error.msg, 'danger');
      }

      this.spinnerCombo.toggle();
    }

  }

}
