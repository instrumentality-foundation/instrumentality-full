import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

import { SpinnerControllerService } from "../services/spinner-controller.service";
import { NotifierService } from "../services/notifier.service";

import { OrchestratorDashboardRoutingModule } from './orchestrator-dashboard-routing.module';
import { OrchestratorDashboardComponent } from './orchestrator-dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HolderComponent } from './holder/holder.component';
import { CommandCardComponent } from './command-card/command-card.component';
import { CompanyCardComponent } from './company-card/company-card.component';
import { NewAssetManualCardComponent } from './new-asset-manual-card/new-asset-manual-card.component';
import { MintAssetCardComponent } from './mint-asset-card/mint-asset-card.component';

@NgModule({
  declarations: [OrchestratorDashboardComponent, NavbarComponent, HolderComponent, CommandCardComponent, CompanyCardComponent, NewAssetManualCardComponent, MintAssetCardComponent],
  imports: [
    CommonModule,
    OrchestratorDashboardRoutingModule,
    FontAwesomeModule,
    FormsModule
  ],
  providers: [SpinnerControllerService, NotifierService]
})
export class OrchestratorDashboardModule { }
