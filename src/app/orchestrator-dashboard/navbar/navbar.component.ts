import { Component, OnInit } from '@angular/core';
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'orchestrator-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  faSignOutAlt = faSignOutAlt;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Deletes token and additional user data from local storage
   */
  public discardToken() {
    let account = JSON.parse(localStorage.getItem('user')).username;
    localStorage.removeItem(account);
    localStorage.removeItem('user');
  }

}
