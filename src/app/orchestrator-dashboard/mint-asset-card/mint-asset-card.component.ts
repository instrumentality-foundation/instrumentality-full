import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommandCardComponent } from '../command-card/command-card.component';
import { NotifierService } from 'src/app/services/notifier.service';
import { SpinnerCombo } from 'src/app/services/spinner-controller.service';
import { IrohaResponse } from '../utils/iroha-command';

@Component({
  selector: 'orchestrator-mint-asset-card',
  templateUrl: './mint-asset-card.component.html',
  styleUrls: ['./mint-asset-card.component.less',
  '../command-card/command-card.component.less']
})
export class MintAssetCardComponent extends CommandCardComponent implements OnInit {

  @ViewChild('frontCard', {static: true}) frontCard : ElementRef; 
  @ViewChild('backCard', {static: true}) backCard : ElementRef;
  @ViewChild('sendButton', {static: true}) sendButton: ElementRef;
  @ViewChild('sendSpinner', {static: true}) sendSpinner: ElementRef;

  assetName: any;
  amount: any;

  private spinnerCombo: SpinnerCombo;

  constructor(
    private notifier: NotifierService
  ) {
    super();
   }

  ngOnInit(): void {
  }

  flipCard() {
    if (this.frontCard.nativeElement.style.display != "none") {
      this.frontCard.nativeElement.style.display = "none";
      this.backCard.nativeElement.style.display = "block";
      this.frontCard.nativeElement.classList.add('uk-animation-scale-down');
    }
    else {
      this.frontCard.nativeElement.style.display = "block";
      this.backCard.nativeElement.style.display = "none";
    }
  }

  async mintAsset() {
    // Check asset name
    if (this.asset_pattern.test(this.assetName) === false) {
      this.notifier.notifyUIkit("Asset name should be small letters, '_' and numbers only", 'danger');
      return;
    }

    // Check amount
    if(this.amount < 0) {
      this.notifier.notifyUIkit('Amount must be greater than 0', 'danger');
      return;
    }

    let account = JSON.parse(localStorage.getItem('user'));

    // Start the spinner combo
    this.spinnerCombo = new SpinnerCombo(this.sendButton, this.sendSpinner);
    this.spinnerCombo.toggle();

    // Start the request
    try {
      let response = await this.command.request(account.username, account.token, this.assetName, this.amount);
      
      let irohaResponse: IrohaResponse = response.body;

      console.log(irohaResponse);
      this.notifier.notifyUIkit(irohaResponse.msg, 'success');
    }
    catch(reason) {
      console.error(reason.error);
      this.notifier.notifyUIkit(reason.error.msg, 'danger');
    }
    finally {
      this.spinnerCombo.toggle();
    }
  }

}
