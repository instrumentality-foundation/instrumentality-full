import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MintAssetCardComponent } from './mint-asset-card.component';

describe('MintAssetCardComponent', () => {
  let component: MintAssetCardComponent;
  let fixture: ComponentFixture<MintAssetCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MintAssetCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MintAssetCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
