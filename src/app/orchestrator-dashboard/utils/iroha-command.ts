import { HttpResponse } from '@angular/common/http';

export interface IrohaCommand {
    name: string;
    description?: string;
    image?: string;

    request: (...args: any[]) => Promise<HttpResponse<IrohaResponse>>;
}

export class IrohaResponse {
    status: string;
    msg: string;
    extra: {};
}