import { IrohaCommand } from './iroha-command';
import { HttpClient } from '@angular/common/http';
import { IrohaResponse } from "../utils/iroha-command";
import { constants } from "../../constants";

export class IrohaCreateAsset implements IrohaCommand {
    
    constructor(
        private http: HttpClient
    ){}
    
    name = "Create new asset";
    description="Creates a new asset on instrumentality domain";
    image="../../../assets/Orchestrator/new_asset.svg"

     /**
     * This request creates new assets inside the "instrumentality" domain
     * 
     * @param account The account id of the orchestrator initiating the request
     * @param token The valid token associated with the account id.
     * @param asset_name The name of the new asset
     * @param precision The number of decimals allowed after dot
     */
    request = (account: string, token: string, asset_name: string, precision: number) => {
        return this.http.post<IrohaResponse>(
            constants.iroha_network + '/asset/create',
            {
                account: account,
                token: token,
                asset_name: asset_name,
                precision: precision
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        )
        .toPromise();
    }

}
