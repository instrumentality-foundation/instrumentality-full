import { IrohaCommand, IrohaResponse } from "./iroha-command";
import { HttpClient } from '@angular/common/http';
import { constants } from "src/app/constants";

export class IrohaCreateCompany implements IrohaCommand {

    constructor (
        private http: HttpClient,
    ){}

    name = "Register Company";
    description = "Create a new company on the blockchain";
    image = "../../../assets/Orchestrator/company.svg"

    request = (account: string, token: string, companyName: string) => {
        return this.http.post<IrohaResponse>(
            constants.iroha_network + '/company/create',
            {
                account: account,
                token: token,
                companyName: companyName
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        )
        .toPromise();
    }

}