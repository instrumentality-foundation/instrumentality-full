import { IrohaCommand, IrohaResponse } from './iroha-command';
import { HttpClient } from '@angular/common/http';
import { constants } from "../../constants";

export class IrohaMintAsset implements IrohaCommand {

    constructor(
        private http: HttpClient
    ) {

    }

    name = "Issue asset";
    description = "Adds desired quantity of an asset to the blockchain";
    image = "../../../assets/Orchestrator/mint_asset.svg";

    /**
     * This request issues an amount of the specified asset in the instrumentality domain.
     * 
     * @param account The account id of the orchestrator initiating the minting
     * @param token The valid token associated with the orchestrator account
     * @param asset_name The unique name of the asset to be issued
     * @param amount The amout that should be issued
     */
    request = (account: string, token: string, asset_name: string, amount: number) => {
        return this.http.post<IrohaResponse>(
            constants.iroha_network + '/asset/mint',
            {
                account: account,
                token: token,
                asset_name: asset_name,
                amount: amount
            },
            {
                observe: 'response',
                responseType: 'json'
            }
        ).toPromise();
    }
}
