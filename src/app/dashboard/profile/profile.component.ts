import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { faPencilAlt, faCameraRetro } from "@fortawesome/free-solid-svg-icons";
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Router } from '@angular/router';
import { constants } from "../../constants";
import { NotifierService } from "../../services/notifier.service";
import { SpinnerCombo } from "../../services/spinner-controller.service";

@Component({
  selector: 'profile-comp',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {

  @ViewChild('saveButton', {static: true}) saveButton: ElementRef;
  @ViewChild('saveSpinner', {static: true}) saveSpinner: ElementRef;
  @ViewChild('modal_profile', {static: true}) modalProfile: ElementRef;
  @ViewChild('profile_image', {static: true}) profileImage: ElementRef;

  public edit_fname: any;
  public edit_sname: any;

  faPencilAlt = faPencilAlt;
  faCameraRetro = faCameraRetro;
  username: string = JSON.parse(localStorage.getItem('user')).username;
  private token: string = JSON.parse(localStorage.getItem('user')).token;

  constructor(
    private notifier: NotifierService,
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    let data = localStorage.getItem(this.username) != null ? JSON.parse(localStorage.getItem(this.username)) : null;
    if (data) {
      this.edit_fname = data.firstname;
      this.edit_sname = data.surname;
    }
    else {
      // We get the data from the blockchain
      let headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');

      this.http.put(
        constants.iroha_network + '/account/get',
        {
          username: this.username,
          token: this.token
        },
        {
          headers: headers,
          responseType: 'json',
          observe: 'response'
        }
      ).toPromise()
      .then(response => {
        console.log(response.body);
        localStorage.setItem(this.username, JSON.stringify({firstname: response.body['FirstName'], surname: response.body['Surname']}));
        this.edit_sname = response.body['Surname'];
        this.edit_fname = response.body['FirstName'];
      })
      .catch( error => {
        console.error(error);
      });
    }
  }

  ngAfterViewInit() : void {
    /** Getting the profile image */
    this.http.put(
      constants.iroha_network + '/account/get/image',
      {
        username: this.username,
        token: this.token
      },
      {
        observe: 'response',
        responseType: 'blob' as 'json'
      }
    ).toPromise()
    .then(response => {
      console.log(response.body);
      this.modalProfile.nativeElement.src = URL.createObjectURL(response.body);
      this.profileImage.nativeElement.src = URL.createObjectURL(response.body);
    })
    .catch(error => {
      console.error(error);
    });
  }

  public setInfo() {
    // Gathering parts of the request body
    let firstname = this.edit_fname != null ? this.edit_fname : '';
    let surname = this.edit_sname != null ? this.edit_sname : '';

    // If there's no token something is up and we redirect to home
    if(localStorage.getItem('user') == null) {
      this.router.navigateByUrl('/landing/home');
      this.notifier.notifyUIkit('Session expired', 'danger');
      return;
    }

    let user = JSON.parse(localStorage.getItem('user')); // Accessing local storage only once

    let account = user.username;
    let token = user.token;

    // Getting a spinner combo
    let spinnerCombo: SpinnerCombo = new SpinnerCombo(this.saveButton, this.saveSpinner);

    let headers : HttpHeaders = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    // HTTP request to python server
    spinnerCombo.toggle();
    this.http.post(
      constants.iroha_network + '/account/set/details',
      {
        account: account,
        token: token,
        firstname: firstname,
        surname: surname
      },
      {
        headers: headers,
        observe: 'response',
        responseType: 'json'
      }
    )
    .toPromise()
    .then(response => {
      console.log(response.body['msg']);
      this.notifier.notifyUIkit(response.body['msg'], 'success');
      localStorage.setItem(account, JSON.stringify({firstname: firstname, surname: surname}));
    })
    .catch(error => {
      console.error(error.error.msg);
      this.notifier.notifyUIkit(error.error.msg, 'danger');
    })
    .finally(() => {
      spinnerCombo.toggle();
    });
  }

  public changeProfilePicture() {
    let input = document.createElement('input');
    input.type = 'file';
    input.accept = "image/svg, image/png, image/jpg, image/jpeg"
    input.click();

    let form_data = new FormData();

    let file : File;
    let user = JSON.parse(localStorage.getItem('user'));
    let username = user.username;
    let token = user.token;

    input.onchange = event => {
      let target = event.target as any;
      file = target.files[0];

      form_data.append('image', file);
      form_data.append('username', username);
      form_data.append('token', token);

      this.http.post(
        constants.iroha_network + '/account/set/image',
        form_data,
        {
          observe: 'response',
          responseType: 'json'
        }
      ).toPromise()
      .then(response => {
        console.log(response.body)
        this.notifier.notifyUIkit('Profile picture set successfully', 'success');

        /** We now do an http request for the image */
        this.http.put(
          constants.iroha_network + '/account/get/image',
          {
            username: username,
            token: token
          },
          {
            observe: 'response',
            responseType: 'blob' as 'json'
          }
        ).toPromise()
        .then(response => {
          console.log(response.body);
          this.modalProfile.nativeElement.src = URL.createObjectURL(response.body);
          this.profileImage.nativeElement.src = URL.createObjectURL(response.body);
        })
        .catch(error => {
          console.error(error);
        });
      })
      .catch(error => {
        this.notifier.notifyUIkit(error.error, 'danger');
        console.error(error.error);
      });
    }

  }

}
