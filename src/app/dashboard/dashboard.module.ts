import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

/* Importing fontawesome module */
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProfileComponent } from './profile/profile.component';
import { SpinnerControllerService } from '../services/spinner-controller.service';
import { NotifierService } from '../services/notifier.service';


@NgModule({
  declarations: [DashboardComponent, NavbarComponent, ProfileComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FontAwesomeModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [SpinnerControllerService, NotifierService]
})
export class DashboardModule { }
