import { Component, OnInit } from '@angular/core';

import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'navbar-comp',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  faSignOutAlt = faSignOutAlt;

  constructor() { }

  ngOnInit(): void {
  }

  public discardToken() {
    let account = JSON.parse(localStorage.getItem('user')).username;
    localStorage.removeItem(account);
    localStorage.removeItem('user');
  }

}
