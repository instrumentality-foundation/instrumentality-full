import { Injectable, ElementRef } from '@angular/core';

/**
 * A class containing a spinner and a button combo.
 * Can switsh between them at will.
 */
export class SpinnerCombo {

  /**
   * Creates a spinner combo with a button and a spinner.
   * @param button The id of the button.
   * @param spinner The id of the spinner.
   */
  constructor(private button: ElementRef, private spinner: ElementRef) {

  }

  /**
   * Toggles the visibility of the each element in the combo.
   * The can't be visible at the same time.
   */
  public toggle() {
    if (this.button.nativeElement.style.display === "none") {
      this.spinner.nativeElement.style.display = "none";
      this.button.nativeElement.style.display = "inline-block";
    }
    else {
      this.spinner.nativeElement.style.display = "inline-block";
      this.button.nativeElement.style.display = "none";
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class SpinnerControllerService {

  constructor() {}

  makeSpinnerCombo(button: ElementRef, spinner: ElementRef) {
    return new SpinnerCombo(button, spinner);
  }
}
