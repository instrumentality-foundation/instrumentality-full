import { Injectable } from '@angular/core';
import { SpinnerCombo } from './spinner-controller.service';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { constants } from "../constants";
import { NotifierService } from "./notifier.service";
import { Router } from '@angular/router';
import * as UIkit from 'uikit';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatorService {

  private account_pattern = /^[a-z_0-9]{1,32}$/;

  constructor(
    private http: HttpClient,
    private notifier: NotifierService,
    private router: Router
  ) { }

  /**
   * This method can tell if an object is trully a spinner combo
   * 
   * @param spinner A combo of button + spinner element
   * @returns True or false
   */
  private isSpinnerCombo(spinnerCombo: SpinnerCombo): boolean {
    if (spinnerCombo instanceof SpinnerCombo === false && spinnerCombo != null) {
      console.log("Trying to pass other object as spinner+button combo");
      return false;
    }

    return true;
  }

  private showKeys (publicKey: string, privateKey: string, username: string) {
    let html =
    `
    <div class="uk-modal-header uk-text-center">
        <h2>Please save these keys in a safe place!</h2>
    </div>
    <div class="uk-modal-body uk-flex uk-flex-column uk-flex-middle uk-flex-center uk-text-center">
        <h3>Your account name:</h3>
        <h3 style="word-break: break-all; font-weight: 700; font-size: 2em;">${username}</h3>
        <h3>Public Key:</h3>
        <span style="word-break: break-all;">${publicKey}</span>
        <h3>Private Key:</h3>
        <span style="word-break: break-all;">${privateKey}</span>
    </div>
    <div class="uk-modal-footer uk-flex uk-flex-middle uk-flex-center">
        <button class="uk-button uk-button-primary uk-modal-close" type="button">I saved the keys</button>
    </div>
    `;
    this.notifier.createModalUIkit(html);
  }

  public createAccount (username: string, modal: HTMLElement, spinnerCombo: SpinnerCombo = null) {
    // Don't pass another object than SpinnerCombo or I'll exit
    if (! this.isSpinnerCombo(spinnerCombo)){
      return;
    }

    if (this.account_pattern.test(username) === true) {
      if(spinnerCombo) {
        spinnerCombo.toggle();
      }

      this.http.get(
        constants.iroha_network + '/account/create/' + username,
        {
          responseType: 'json',
          observe: 'response'
        }
        )
        .toPromise()
        .then(response => {
          console.log(response.body);
          let body = response.body as any;
          this.notifier.notifyUIkit("Account created successfully", "success");
          this.showKeys(body.public_key, body.private_key, username+"@Instrumentality");
          UIkit.modal(modal).hide();
        })
        .catch(reason => {
          console.log(reason);
          this.notifier.notifyUIkit(reason.error.msg, "danger");
        })
        .finally(() => {spinnerCombo.toggle();});
    }
    else {
      this.notifier.notifyUIkit("Use only small letters, numbers and _", "danger");
      console.error("Wrong username format");
    }
  }

  public async authenticate (username: string, privateKey: string, spinnerCombo: SpinnerCombo = null) {
    // Don't pass another object than SpinnerCombo or I'll exit
    if (spinnerCombo instanceof SpinnerCombo === false && spinnerCombo != null) {
      console.error("Bad options! Not going any further!");
      return -1;
    }

    if(spinnerCombo) {
      spinnerCombo.toggle();
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type', 'application/json')

    await this.http.post(
      constants.iroha_network + '/account/auth',
      {
        username: username,
        privateKey: privateKey
      },
      {
        headers: headers,
        observe: 'response',
        responseType: 'json'
      }
    )
    .toPromise()
    .then(response => {
      console.log(response.body);
      let body = response.body;
      
      localStorage.setItem('user', JSON.stringify({username: username, token: body['token']}));

      this.notifier.notifyUIkit(body['msg'], "success");

      if (body['orchestrator']) {
        this.router.navigateByUrl("/orchestrator");
        console.log('ORCHESTRA')
      }
      else {
        this.router.navigateByUrl("/" + username + "/dashboard");
      }
    })
    .catch (reason => {
      console.error(reason);
      this.notifier.notifyUIkit(reason.error.msg, "danger");
    })
    .finally(() => {
      spinnerCombo.toggle();
    });
  }
}
