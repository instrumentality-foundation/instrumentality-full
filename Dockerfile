FROM node:alpine

# Installing yarn, serve static server and svgo for svg optimisations
RUN npm install yarn
RUN yarn global add serve svgo

# Setting the working directory
WORKDIR /instrumentality

COPY package.json ./
COPY yarn.lock ./

# Install all dependencies
RUN yarn install

# Copy project files
COPY . .

# Run svgo on all assets
RUN svgo -r -f ./src/assets/

# Build app
RUN yarn build

# Port for serving the app
EXPOSE 8080

# Serve the site
CMD [ "serve", "-l", "8080", "-s", "-C", "dist/instrumentality-full" ]